import { importType } from '@angular/compiler/src/output/output_ast';
import { ReactiveFormsModule } from '@angular/forms';
import { FormGroup, FormControl, FormControlName } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {

  registerForm = new FormGroup({
    name: new FormControl(''),
    collegeName: new FormControl(''),
    city: new FormControl(''),
    email: new FormControl(''),
    phoneNo: new FormControl(''),
    designation: new FormControl(''),
    amtToPay: new FormControl(''),
    proofOfPayment: new FormControl(''),
  })

  onChange(){
    console.log('check');
    
    switch(this.registerForm.value.designation){
      case "research": {
        this.registerForm.value.amtToPay = '100';
        break;
      }

      case "faculty": {
        this.registerForm.value.amtToPay = '200';
        break;
      }

      case "industryExp": {
        this.registerForm.value.amtToPay = '300';
        break;
      }
    }
  }

  onSubmit(){
    console.log(this.registerForm.value);
  }

  constructor() { }

  ngOnInit(): void {
  }

}

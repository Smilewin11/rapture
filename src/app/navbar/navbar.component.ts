import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  navLinks = [
    {
      title: 'Home',
      link: '/home'
    },
    {
      title: 'About Us',
      link: '/about-us'
    },
    {
      title: 'Speakers',
      link: '/speakers'
    },
    {
      title: 'Schedule',
      link: '/schedule',
    },
    {
      title: 'Register',
      link: '/register'
    },
    
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
